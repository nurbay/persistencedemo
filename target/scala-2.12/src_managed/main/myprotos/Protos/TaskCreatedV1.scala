// Generated by the Scala Plugin for the Protocol Buffer Compiler.
// Do not edit!
//
// Protofile syntax: PROTO2

package myprotos.Protos

@SerialVersionUID(0L)
final case class TaskCreatedV1(
    taskName: _root_.scala.Predef.String
    ) extends scalapb.GeneratedMessage with scalapb.Message[TaskCreatedV1] with scalapb.lenses.Updatable[TaskCreatedV1] {
    @transient
    private[this] var __serializedSizeCachedValue: _root_.scala.Int = 0
    private[this] def __computeSerializedValue(): _root_.scala.Int = {
      var __size = 0
      
      {
        val __value = taskName
        __size += _root_.com.google.protobuf.CodedOutputStream.computeStringSize(1, __value)
      };
      __size
    }
    final override def serializedSize: _root_.scala.Int = {
      var read = __serializedSizeCachedValue
      if (read == 0) {
        read = __computeSerializedValue()
        __serializedSizeCachedValue = read
      }
      read
    }
    def writeTo(`_output__`: _root_.com.google.protobuf.CodedOutputStream): _root_.scala.Unit = {
      
      {
        val __v = taskName
        _output__.writeString(1, __v)
      };
    }
    def mergeFrom(`_input__`: _root_.com.google.protobuf.CodedInputStream): myprotos.Protos.TaskCreatedV1 = {
      var __taskName = this.taskName
      var __requiredFields0: _root_.scala.Long = 0x1L
      var _done__ = false
      while (!_done__) {
        val _tag__ = _input__.readTag()
        _tag__ match {
          case 0 => _done__ = true
          case 10 =>
            __taskName = _input__.readString()
            __requiredFields0 &= 0xfffffffffffffffeL
          case tag => _input__.skipField(tag)
        }
      }
      if (__requiredFields0 != 0L) { throw new _root_.com.google.protobuf.InvalidProtocolBufferException("Message missing required fields.") } 
      myprotos.Protos.TaskCreatedV1(
          taskName = __taskName
      )
    }
    def withTaskName(__v: _root_.scala.Predef.String): TaskCreatedV1 = copy(taskName = __v)
    def getFieldByNumber(__fieldNumber: _root_.scala.Int): _root_.scala.Any = {
      (__fieldNumber: @_root_.scala.unchecked) match {
        case 1 => taskName
      }
    }
    def getField(__field: _root_.scalapb.descriptors.FieldDescriptor): _root_.scalapb.descriptors.PValue = {
      _root_.scala.Predef.require(__field.containingMessage eq companion.scalaDescriptor)
      (__field.number: @_root_.scala.unchecked) match {
        case 1 => _root_.scalapb.descriptors.PString(taskName)
      }
    }
    def toProtoString: _root_.scala.Predef.String = _root_.scalapb.TextFormat.printToUnicodeString(this)
    def companion = myprotos.Protos.TaskCreatedV1
}

object TaskCreatedV1 extends scalapb.GeneratedMessageCompanion[myprotos.Protos.TaskCreatedV1] {
  implicit def messageCompanion: scalapb.GeneratedMessageCompanion[myprotos.Protos.TaskCreatedV1] = this
  def fromFieldsMap(__fieldsMap: scala.collection.immutable.Map[_root_.com.google.protobuf.Descriptors.FieldDescriptor, _root_.scala.Any]): myprotos.Protos.TaskCreatedV1 = {
    _root_.scala.Predef.require(__fieldsMap.keys.forall(_.getContainingType() == javaDescriptor), "FieldDescriptor does not match message type.")
    val __fields = javaDescriptor.getFields
    myprotos.Protos.TaskCreatedV1(
      __fieldsMap(__fields.get(0)).asInstanceOf[_root_.scala.Predef.String]
    )
  }
  implicit def messageReads: _root_.scalapb.descriptors.Reads[myprotos.Protos.TaskCreatedV1] = _root_.scalapb.descriptors.Reads{
    case _root_.scalapb.descriptors.PMessage(__fieldsMap) =>
      _root_.scala.Predef.require(__fieldsMap.keys.forall(_.containingMessage == scalaDescriptor), "FieldDescriptor does not match message type.")
      myprotos.Protos.TaskCreatedV1(
        __fieldsMap.get(scalaDescriptor.findFieldByNumber(1).get).get.as[_root_.scala.Predef.String]
      )
    case _ => throw new RuntimeException("Expected PMessage")
  }
  def javaDescriptor: _root_.com.google.protobuf.Descriptors.Descriptor = ProtosProto.javaDescriptor.getMessageTypes.get(0)
  def scalaDescriptor: _root_.scalapb.descriptors.Descriptor = ProtosProto.scalaDescriptor.messages(0)
  def messageCompanionForFieldNumber(__number: _root_.scala.Int): _root_.scalapb.GeneratedMessageCompanion[_] = throw new MatchError(__number)
  lazy val nestedMessagesCompanions: Seq[_root_.scalapb.GeneratedMessageCompanion[_]] = Seq.empty
  def enumCompanionForFieldNumber(__fieldNumber: _root_.scala.Int): _root_.scalapb.GeneratedEnumCompanion[_] = throw new MatchError(__fieldNumber)
  lazy val defaultInstance = myprotos.Protos.TaskCreatedV1(
    taskName = ""
  )
  implicit class TaskCreatedV1Lens[UpperPB](_l: _root_.scalapb.lenses.Lens[UpperPB, myprotos.Protos.TaskCreatedV1]) extends _root_.scalapb.lenses.ObjectLens[UpperPB, myprotos.Protos.TaskCreatedV1](_l) {
    def taskName: _root_.scalapb.lenses.Lens[UpperPB, _root_.scala.Predef.String] = field(_.taskName)((c_, f_) => c_.copy(taskName = f_))
  }
  final val TASKNAME_FIELD_NUMBER = 1
  def of(
    taskName: _root_.scala.Predef.String
  ): _root_.myprotos.Protos.TaskCreatedV1 = _root_.myprotos.Protos.TaskCreatedV1(
    taskName
  )
}
