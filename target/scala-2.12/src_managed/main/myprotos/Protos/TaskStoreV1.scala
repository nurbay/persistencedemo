// Generated by the Scala Plugin for the Protocol Buffer Compiler.
// Do not edit!
//
// Protofile syntax: PROTO2

package myprotos.Protos

@SerialVersionUID(0L)
final case class TaskStoreV1(
    tasks: _root_.scala.collection.Seq[_root_.scala.Predef.String] = _root_.scala.collection.Seq.empty
    ) extends scalapb.GeneratedMessage with scalapb.Message[TaskStoreV1] with scalapb.lenses.Updatable[TaskStoreV1] {
    @transient
    private[this] var __serializedSizeCachedValue: _root_.scala.Int = 0
    private[this] def __computeSerializedValue(): _root_.scala.Int = {
      var __size = 0
      tasks.foreach { __item =>
        val __value = __item
        __size += _root_.com.google.protobuf.CodedOutputStream.computeStringSize(1, __value)
      }
      __size
    }
    final override def serializedSize: _root_.scala.Int = {
      var read = __serializedSizeCachedValue
      if (read == 0) {
        read = __computeSerializedValue()
        __serializedSizeCachedValue = read
      }
      read
    }
    def writeTo(`_output__`: _root_.com.google.protobuf.CodedOutputStream): _root_.scala.Unit = {
      tasks.foreach { __v =>
        val __m = __v
        _output__.writeString(1, __m)
      };
    }
    def mergeFrom(`_input__`: _root_.com.google.protobuf.CodedInputStream): myprotos.Protos.TaskStoreV1 = {
      val __tasks = (_root_.scala.collection.immutable.Vector.newBuilder[_root_.scala.Predef.String] ++= this.tasks)
      var _done__ = false
      while (!_done__) {
        val _tag__ = _input__.readTag()
        _tag__ match {
          case 0 => _done__ = true
          case 10 =>
            __tasks += _input__.readString()
          case tag => _input__.skipField(tag)
        }
      }
      myprotos.Protos.TaskStoreV1(
          tasks = __tasks.result()
      )
    }
    def clearTasks = copy(tasks = _root_.scala.collection.Seq.empty)
    def addTasks(__vs: _root_.scala.Predef.String*): TaskStoreV1 = addAllTasks(__vs)
    def addAllTasks(__vs: TraversableOnce[_root_.scala.Predef.String]): TaskStoreV1 = copy(tasks = tasks ++ __vs)
    def withTasks(__v: _root_.scala.collection.Seq[_root_.scala.Predef.String]): TaskStoreV1 = copy(tasks = __v)
    def getFieldByNumber(__fieldNumber: _root_.scala.Int): _root_.scala.Any = {
      (__fieldNumber: @_root_.scala.unchecked) match {
        case 1 => tasks
      }
    }
    def getField(__field: _root_.scalapb.descriptors.FieldDescriptor): _root_.scalapb.descriptors.PValue = {
      _root_.scala.Predef.require(__field.containingMessage eq companion.scalaDescriptor)
      (__field.number: @_root_.scala.unchecked) match {
        case 1 => _root_.scalapb.descriptors.PRepeated(tasks.map(_root_.scalapb.descriptors.PString)(_root_.scala.collection.breakOut))
      }
    }
    def toProtoString: _root_.scala.Predef.String = _root_.scalapb.TextFormat.printToUnicodeString(this)
    def companion = myprotos.Protos.TaskStoreV1
}

object TaskStoreV1 extends scalapb.GeneratedMessageCompanion[myprotos.Protos.TaskStoreV1] {
  implicit def messageCompanion: scalapb.GeneratedMessageCompanion[myprotos.Protos.TaskStoreV1] = this
  def fromFieldsMap(__fieldsMap: scala.collection.immutable.Map[_root_.com.google.protobuf.Descriptors.FieldDescriptor, _root_.scala.Any]): myprotos.Protos.TaskStoreV1 = {
    _root_.scala.Predef.require(__fieldsMap.keys.forall(_.getContainingType() == javaDescriptor), "FieldDescriptor does not match message type.")
    val __fields = javaDescriptor.getFields
    myprotos.Protos.TaskStoreV1(
      __fieldsMap.getOrElse(__fields.get(0), Nil).asInstanceOf[_root_.scala.collection.Seq[_root_.scala.Predef.String]]
    )
  }
  implicit def messageReads: _root_.scalapb.descriptors.Reads[myprotos.Protos.TaskStoreV1] = _root_.scalapb.descriptors.Reads{
    case _root_.scalapb.descriptors.PMessage(__fieldsMap) =>
      _root_.scala.Predef.require(__fieldsMap.keys.forall(_.containingMessage == scalaDescriptor), "FieldDescriptor does not match message type.")
      myprotos.Protos.TaskStoreV1(
        __fieldsMap.get(scalaDescriptor.findFieldByNumber(1).get).map(_.as[_root_.scala.collection.Seq[_root_.scala.Predef.String]]).getOrElse(_root_.scala.collection.Seq.empty)
      )
    case _ => throw new RuntimeException("Expected PMessage")
  }
  def javaDescriptor: _root_.com.google.protobuf.Descriptors.Descriptor = ProtosProto.javaDescriptor.getMessageTypes.get(2)
  def scalaDescriptor: _root_.scalapb.descriptors.Descriptor = ProtosProto.scalaDescriptor.messages(2)
  def messageCompanionForFieldNumber(__number: _root_.scala.Int): _root_.scalapb.GeneratedMessageCompanion[_] = throw new MatchError(__number)
  lazy val nestedMessagesCompanions: Seq[_root_.scalapb.GeneratedMessageCompanion[_]] = Seq.empty
  def enumCompanionForFieldNumber(__fieldNumber: _root_.scala.Int): _root_.scalapb.GeneratedEnumCompanion[_] = throw new MatchError(__fieldNumber)
  lazy val defaultInstance = myprotos.Protos.TaskStoreV1(
  )
  implicit class TaskStoreV1Lens[UpperPB](_l: _root_.scalapb.lenses.Lens[UpperPB, myprotos.Protos.TaskStoreV1]) extends _root_.scalapb.lenses.ObjectLens[UpperPB, myprotos.Protos.TaskStoreV1](_l) {
    def tasks: _root_.scalapb.lenses.Lens[UpperPB, _root_.scala.collection.Seq[_root_.scala.Predef.String]] = field(_.tasks)((c_, f_) => c_.copy(tasks = f_))
  }
  final val TASKS_FIELD_NUMBER = 1
  def of(
    tasks: _root_.scala.collection.Seq[_root_.scala.Predef.String]
  ): _root_.myprotos.Protos.TaskStoreV1 = _root_.myprotos.Protos.TaskStoreV1(
    tasks
  )
}
