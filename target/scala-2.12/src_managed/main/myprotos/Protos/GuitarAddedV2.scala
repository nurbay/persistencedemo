// Generated by the Scala Plugin for the Protocol Buffer Compiler.
// Do not edit!
//
// Protofile syntax: PROTO2

package myprotos.Protos

@SerialVersionUID(0L)
final case class GuitarAddedV2(
    id: _root_.scala.Predef.String,
    model: _root_.scala.Predef.String,
    make: _root_.scala.Predef.String,
    quantity: _root_.scala.Int,
    guitarType: _root_.scala.Predef.String
    ) extends scalapb.GeneratedMessage with scalapb.Message[GuitarAddedV2] with scalapb.lenses.Updatable[GuitarAddedV2] {
    @transient
    private[this] var __serializedSizeCachedValue: _root_.scala.Int = 0
    private[this] def __computeSerializedValue(): _root_.scala.Int = {
      var __size = 0
      
      {
        val __value = id
        __size += _root_.com.google.protobuf.CodedOutputStream.computeStringSize(1, __value)
      };
      
      {
        val __value = model
        __size += _root_.com.google.protobuf.CodedOutputStream.computeStringSize(2, __value)
      };
      
      {
        val __value = make
        __size += _root_.com.google.protobuf.CodedOutputStream.computeStringSize(3, __value)
      };
      
      {
        val __value = quantity
        __size += _root_.com.google.protobuf.CodedOutputStream.computeInt32Size(5, __value)
      };
      
      {
        val __value = guitarType
        __size += _root_.com.google.protobuf.CodedOutputStream.computeStringSize(6, __value)
      };
      __size
    }
    final override def serializedSize: _root_.scala.Int = {
      var read = __serializedSizeCachedValue
      if (read == 0) {
        read = __computeSerializedValue()
        __serializedSizeCachedValue = read
      }
      read
    }
    def writeTo(`_output__`: _root_.com.google.protobuf.CodedOutputStream): _root_.scala.Unit = {
      
      {
        val __v = id
        _output__.writeString(1, __v)
      };
      
      {
        val __v = model
        _output__.writeString(2, __v)
      };
      
      {
        val __v = make
        _output__.writeString(3, __v)
      };
      
      {
        val __v = quantity
        _output__.writeInt32(5, __v)
      };
      
      {
        val __v = guitarType
        _output__.writeString(6, __v)
      };
    }
    def mergeFrom(`_input__`: _root_.com.google.protobuf.CodedInputStream): myprotos.Protos.GuitarAddedV2 = {
      var __id = this.id
      var __model = this.model
      var __make = this.make
      var __quantity = this.quantity
      var __guitarType = this.guitarType
      var __requiredFields0: _root_.scala.Long = 0x1fL
      var _done__ = false
      while (!_done__) {
        val _tag__ = _input__.readTag()
        _tag__ match {
          case 0 => _done__ = true
          case 10 =>
            __id = _input__.readString()
            __requiredFields0 &= 0xfffffffffffffffeL
          case 18 =>
            __model = _input__.readString()
            __requiredFields0 &= 0xfffffffffffffffdL
          case 26 =>
            __make = _input__.readString()
            __requiredFields0 &= 0xfffffffffffffffbL
          case 40 =>
            __quantity = _input__.readInt32()
            __requiredFields0 &= 0xfffffffffffffff7L
          case 50 =>
            __guitarType = _input__.readString()
            __requiredFields0 &= 0xffffffffffffffefL
          case tag => _input__.skipField(tag)
        }
      }
      if (__requiredFields0 != 0L) { throw new _root_.com.google.protobuf.InvalidProtocolBufferException("Message missing required fields.") } 
      myprotos.Protos.GuitarAddedV2(
          id = __id,
          model = __model,
          make = __make,
          quantity = __quantity,
          guitarType = __guitarType
      )
    }
    def withId(__v: _root_.scala.Predef.String): GuitarAddedV2 = copy(id = __v)
    def withModel(__v: _root_.scala.Predef.String): GuitarAddedV2 = copy(model = __v)
    def withMake(__v: _root_.scala.Predef.String): GuitarAddedV2 = copy(make = __v)
    def withQuantity(__v: _root_.scala.Int): GuitarAddedV2 = copy(quantity = __v)
    def withGuitarType(__v: _root_.scala.Predef.String): GuitarAddedV2 = copy(guitarType = __v)
    def getFieldByNumber(__fieldNumber: _root_.scala.Int): _root_.scala.Any = {
      (__fieldNumber: @_root_.scala.unchecked) match {
        case 1 => id
        case 2 => model
        case 3 => make
        case 5 => quantity
        case 6 => guitarType
      }
    }
    def getField(__field: _root_.scalapb.descriptors.FieldDescriptor): _root_.scalapb.descriptors.PValue = {
      _root_.scala.Predef.require(__field.containingMessage eq companion.scalaDescriptor)
      (__field.number: @_root_.scala.unchecked) match {
        case 1 => _root_.scalapb.descriptors.PString(id)
        case 2 => _root_.scalapb.descriptors.PString(model)
        case 3 => _root_.scalapb.descriptors.PString(make)
        case 5 => _root_.scalapb.descriptors.PInt(quantity)
        case 6 => _root_.scalapb.descriptors.PString(guitarType)
      }
    }
    def toProtoString: _root_.scala.Predef.String = _root_.scalapb.TextFormat.printToUnicodeString(this)
    def companion = myprotos.Protos.GuitarAddedV2
}

object GuitarAddedV2 extends scalapb.GeneratedMessageCompanion[myprotos.Protos.GuitarAddedV2] {
  implicit def messageCompanion: scalapb.GeneratedMessageCompanion[myprotos.Protos.GuitarAddedV2] = this
  def fromFieldsMap(__fieldsMap: scala.collection.immutable.Map[_root_.com.google.protobuf.Descriptors.FieldDescriptor, _root_.scala.Any]): myprotos.Protos.GuitarAddedV2 = {
    _root_.scala.Predef.require(__fieldsMap.keys.forall(_.getContainingType() == javaDescriptor), "FieldDescriptor does not match message type.")
    val __fields = javaDescriptor.getFields
    myprotos.Protos.GuitarAddedV2(
      __fieldsMap(__fields.get(0)).asInstanceOf[_root_.scala.Predef.String],
      __fieldsMap(__fields.get(1)).asInstanceOf[_root_.scala.Predef.String],
      __fieldsMap(__fields.get(2)).asInstanceOf[_root_.scala.Predef.String],
      __fieldsMap(__fields.get(3)).asInstanceOf[_root_.scala.Int],
      __fieldsMap(__fields.get(4)).asInstanceOf[_root_.scala.Predef.String]
    )
  }
  implicit def messageReads: _root_.scalapb.descriptors.Reads[myprotos.Protos.GuitarAddedV2] = _root_.scalapb.descriptors.Reads{
    case _root_.scalapb.descriptors.PMessage(__fieldsMap) =>
      _root_.scala.Predef.require(__fieldsMap.keys.forall(_.containingMessage == scalaDescriptor), "FieldDescriptor does not match message type.")
      myprotos.Protos.GuitarAddedV2(
        __fieldsMap.get(scalaDescriptor.findFieldByNumber(1).get).get.as[_root_.scala.Predef.String],
        __fieldsMap.get(scalaDescriptor.findFieldByNumber(2).get).get.as[_root_.scala.Predef.String],
        __fieldsMap.get(scalaDescriptor.findFieldByNumber(3).get).get.as[_root_.scala.Predef.String],
        __fieldsMap.get(scalaDescriptor.findFieldByNumber(5).get).get.as[_root_.scala.Int],
        __fieldsMap.get(scalaDescriptor.findFieldByNumber(6).get).get.as[_root_.scala.Predef.String]
      )
    case _ => throw new RuntimeException("Expected PMessage")
  }
  def javaDescriptor: _root_.com.google.protobuf.Descriptors.Descriptor = ProtosProto.javaDescriptor.getMessageTypes.get(4)
  def scalaDescriptor: _root_.scalapb.descriptors.Descriptor = ProtosProto.scalaDescriptor.messages(4)
  def messageCompanionForFieldNumber(__number: _root_.scala.Int): _root_.scalapb.GeneratedMessageCompanion[_] = throw new MatchError(__number)
  lazy val nestedMessagesCompanions: Seq[_root_.scalapb.GeneratedMessageCompanion[_]] = Seq.empty
  def enumCompanionForFieldNumber(__fieldNumber: _root_.scala.Int): _root_.scalapb.GeneratedEnumCompanion[_] = throw new MatchError(__fieldNumber)
  lazy val defaultInstance = myprotos.Protos.GuitarAddedV2(
    id = "",
    model = "",
    make = "",
    quantity = 0,
    guitarType = ""
  )
  implicit class GuitarAddedV2Lens[UpperPB](_l: _root_.scalapb.lenses.Lens[UpperPB, myprotos.Protos.GuitarAddedV2]) extends _root_.scalapb.lenses.ObjectLens[UpperPB, myprotos.Protos.GuitarAddedV2](_l) {
    def id: _root_.scalapb.lenses.Lens[UpperPB, _root_.scala.Predef.String] = field(_.id)((c_, f_) => c_.copy(id = f_))
    def model: _root_.scalapb.lenses.Lens[UpperPB, _root_.scala.Predef.String] = field(_.model)((c_, f_) => c_.copy(model = f_))
    def make: _root_.scalapb.lenses.Lens[UpperPB, _root_.scala.Predef.String] = field(_.make)((c_, f_) => c_.copy(make = f_))
    def quantity: _root_.scalapb.lenses.Lens[UpperPB, _root_.scala.Int] = field(_.quantity)((c_, f_) => c_.copy(quantity = f_))
    def guitarType: _root_.scalapb.lenses.Lens[UpperPB, _root_.scala.Predef.String] = field(_.guitarType)((c_, f_) => c_.copy(guitarType = f_))
  }
  final val ID_FIELD_NUMBER = 1
  final val MODEL_FIELD_NUMBER = 2
  final val MAKE_FIELD_NUMBER = 3
  final val QUANTITY_FIELD_NUMBER = 5
  final val GUITARTYPE_FIELD_NUMBER = 6
  def of(
    id: _root_.scala.Predef.String,
    model: _root_.scala.Predef.String,
    make: _root_.scala.Predef.String,
    quantity: _root_.scala.Int,
    guitarType: _root_.scala.Predef.String
  ): _root_.myprotos.Protos.GuitarAddedV2 = _root_.myprotos.Protos.GuitarAddedV2(
    id,
    model,
    make,
    quantity,
    guitarType
  )
}
