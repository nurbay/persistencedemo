name := "PersistenceDemo"

version := "0.1"

scalaVersion := "2.12.8"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.5.6",
  "com.typesafe.akka" %% "akka-persistence" % "2.5.6",
  "com.typesafe.akka" %% "akka-persistence-query" % "2.5.6",
  "com.typesafe.akka" %% "akka-persistence-cassandra" % "0.59",
  "com.typesafe.akka" %% "akka-persistence-cassandra-launcher" % "0.59" % Test,
  "com.thesamet.scalapb" %% "scalapb-runtime" % scalapb.compiler.Version.scalapbVersion % "protobuf"
)

PB.targets in Compile := Seq(
  scalapb.gen() -> (sourceManaged in Compile).value
//  scalapb.gen(flatPackage = true, grpc = false) -> (sourceManaged in Compile).value
)
