package util

import akka.actor.ExtendedActorSystem
import akka.persistence.journal._
import com.typesafe.config.ConfigFactory
import myprotos.Protos.{TaskCreatedV1, TaskCreatedV2}


class MyCustomReadAdapter(system: ExtendedActorSystem) extends ReadEventAdapter {

  val config = ConfigFactory.load().getConfig("custom-serializer")

  override def fromJournal(event: Any, manifest: String): EventSeq = event match {
    case c: TaskCreatedV1 => EventSeq.single(TaskCreatedV2(c.taskName, None))
    case other => EventSeq.single(other)

  }

}

//    override def toJournal(event: Any): Any = event match {
//      case c: TaskCreatedV1 => Tagged(c, Set("sometag"))
//      case e: ExampleEvent => Tagged(ExampleEvent1(e.evnts), Set("sometag2"))
//    }
