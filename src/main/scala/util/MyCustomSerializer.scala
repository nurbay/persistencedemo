package util

import akka.serialization.SerializerWithStringManifest
import myprotos.Protos.{TaskCreatedV1, TaskStoreV1}


class MyCustomSerializer extends SerializerWithStringManifest {

  final val TaskCreatedV1Manifest = classOf[TaskCreatedV1].getName
  final val TaskStoreV1Manifest = classOf[TaskStoreV1].getName

  //val utf8 = Charset.forName("UTF-8")

  override def identifier: Int = 101

  // Event <- **Deserializer** <- Serialized(Event) <- Journal
  override def fromBinary(bytes: Array[Byte], manifest: String): AnyRef = manifest match {

    case TaskCreatedV1Manifest => TaskCreatedV1.parseFrom(bytes)
    case TaskStoreV1Manifest => TaskStoreV1.parseFrom(bytes)

  }
  // We use the manifest to determine the event (it is called for us during serializing)
  // Akka will call manifest and attach it to the message in the event journal/snapshot database
  // when toBinary is being invoked
  override def manifest(o: AnyRef): String = o.getClass.getName

  // Event -> **Serializer** -> Serialized(Event) -> Journal
  override def toBinary(o: AnyRef): Array[Byte] = o match {
    case tc: TaskCreatedV1 => tc.toByteArray
    case ts: TaskStoreV1 => ts.toByteArray
  }

}
