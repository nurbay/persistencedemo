//package core
//
//import akka.actor.{ActorSystem, Props, _}
//import akka.persistence._
//import com.typesafe.config.ConfigFactory
//import myprotos.Protos.TaskCreatedV2
//
//import scala.io.StdIn
//
//
//case class TaskManagerActor3() extends PersistentActor with ActorLogging {
//
//  import TaskManagerActor3._
//
//  var tasksStore: List[TaskCreatedV2] = List.empty
//
//
//  override val persistenceId = "task-manager-3"
//
//  override def receiveRecover: Receive = {
//
//    case t@TaskCreatedV2(task,_) =>
//
//      log.info("--> RECOVERING EVENT: [{}]", t)
//      tasksStore = t :: tasksStore
//
//  }
//
//  override def receiveCommand: Receive = {
//
//    case a@AddTask(task) =>
//
//      log.info(s"Received add task: $task")
//      persist(TaskCreatedV2(task, Some("assignee"))) { event =>
//        tasksStore = event :: tasksStore
//      }
//
//    case "print" => println(s"Taskstore: ${tasksStore.reverse}")
//
//  }
//
//}
//
//object TaskManagerActor3 {
//
//  case class AddTask(taskName: String)
//
//}
//
//object TaskManagerActorDemo3 extends App {
//
//  val system = ActorSystem("PersistenceSystem", ConfigFactory.load().getConfig("custom-serializer"))
//  val persistentActor = system.actorOf(Props[TaskManagerActor3])
//
//  //  persistentActor ! "print"
//  //    persistentActor ! AddTask("task-33")
//  //    persistentActor ! AddTask("task-31")
//  persistentActor ! "print"
//  //  persistentActor ! AddTask("task-13")
//  //  persistentActor ! "snapshot"
//  //  persistentActor ! AddTask("task-14")
//  //  persistentActor ! "snapshot"
//  //  persistentActor ! "print"
//
//  StdIn.readLine()
//  system.terminate()
//
//}