package core

import akka.actor.{ActorSystem, Props, _}
import akka.persistence._
import com.typesafe.config.ConfigFactory
import core.TaskManagerActor2.AddTask
import myprotos.Protos.{TaskCreatedV1, TaskStoreV1}

import scala.io.StdIn

case class TaskManagerActor2() extends PersistentActor with ActorLogging {

  import TaskManagerActor2._

  var tasksStore = TaskStoreV1()

  override val persistenceId = "task-manager-21"

  override def receiveRecover: Receive = {

    case t@TaskCreatedV1(task) =>

      log.info("--> RECOVERING EVENT: [{}]", t)
      val newList = task :: tasksStore.tasks.toList
      tasksStore = TaskStoreV1(newList)


//    case SnapshotOffer(_, ts@TaskStoreV1(_)) =>
//      log.info("--> RECOVERING FROM SNAPSHOT: [{}]", ts)
//      tasksStore = ts

  }

  override def receiveCommand: Receive = {

    case a@AddTask(task) =>

      log.info(s"Received add task: $task")
      persist(TaskCreatedV1(task)) { event =>
        val newList = task :: tasksStore.tasks.toList
        tasksStore = TaskStoreV1(newList)
      }

    case "print" => println(tasksStore.tasks.reverse)

    case "snapshot" => saveSnapshot(TaskStoreV1(tasksStore.tasks))

    case SaveSnapshotSuccess(m) => log.info(s"Snapshot saved : $m")

    case SaveSnapshotFailure(metadata, cause) => log.warning(s"Failed to save snapshot : {}", cause)
  }

}

object TaskManagerActor2 {

  case class AddTask(taskName: String)

}

object TaskManagerActorDemo2 extends App {

  val system = ActorSystem("PersistenceSystem", ConfigFactory.load().getConfig("custom-serializer"))
  val persistentActor = system.actorOf(Props[TaskManagerActor2])

  persistentActor ! "print"
  persistentActor ! AddTask("task-demo2-1")
  persistentActor ! AddTask("task-demo2-2")
  persistentActor ! AddTask("task-demo2-3")
//  persistentActor ! "snapshot"
//  persistentActor ! AddTask("task-14")
//  persistentActor ! "snapshot"
//  persistentActor ! "print"

  StdIn.readLine()
  system.terminate()

}