package core

import akka.actor.{ActorSystem, Props, _}
import akka.persistence._
import com.typesafe.config.ConfigFactory
import core.TaskManagerActor.AddTask

import scala.io.StdIn

case class TaskManagerActor() extends PersistentActor with ActorLogging {

  import TaskManagerActor._

  override val persistenceId = "task-manager"

  var tasksStore = TaskStore()

  def updateState(task: String): Unit = {
    tasksStore = tasksStore.updated(task)
  }

  override def receiveRecover: Receive = {

    case t@TaskCreated(task) =>

      log.info("--> RECOVERING EVENT: [{}]", t)
      updateState(task)


    case SnapshotOffer(_, ts: TaskStore) =>
      log.info("--> RECOVERING FROM SNAPSHOT: [{}]", ts)
      tasksStore = ts

    //      log.info("Recovered from snapshot: {}", e)
    //      state = ExampleEvent(e.events.toList)


  }

  override def onPersistFailure(cause: Throwable, event: Any, seqNr: Long): Unit = super.onPersistFailure(cause, event, seqNr)

  override def onPersistRejected(cause: Throwable, event: Any, seqNr: Long): Unit = super.onPersistRejected(cause, event, seqNr)

  override def onRecoveryFailure(cause: Throwable, event: Option[Any]): Unit = super.onRecoveryFailure(cause, event)

  override def receiveCommand: Receive = {

    case a@AddTask(task) =>

      log.info(s"Received add task: $task")
      persist(TaskCreated(task)) { event =>
        updateState(task)
      }

    case "print" => println(tasksStore.tasks)

    case "snapshot" => saveSnapshot(TaskStore(tasksStore.tasks))

    case SaveSnapshotSuccess(m) => log.info(s"Snapshot saved : $m")

//    case SaveSnapshotFailure(metadata, cause) => log.warning(s"Failed to save snapshot : {}", cause)
  }

}

object TaskManagerActor {

  case class AddTask(taskName: String)

  case class TaskCreated(taskName: String)

  case class TaskStore(var tasks: List[String] = Nil){

    def updated(name: String) = {

      val newList = name :: tasks
      copy(newList)
    }

    override def toString: String = tasks.reverse.toString()

//    def size = tasks.length

  }

}

object TaskManagerActorDemo extends App {

  val system = ActorSystem("PersistenceSystem", ConfigFactory.load().getConfig("demo1"))
  val persistentActor = system.actorOf(Props[TaskManagerActor])

  persistentActor ! "print"
//    persistentActor ! AddTask("task1")
//    persistentActor ! AddTask("task5")
//    persistentActor ! "snapshot"
//    persistentActor ! AddTask("task4")
  //  persistentActor ! "print"

  StdIn.readLine()
  system.terminate()

}