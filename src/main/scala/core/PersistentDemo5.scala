package core

import akka.actor.{ActorLogging, ActorSystem, Props}
import akka.persistence.PersistentActor
import akka.persistence.cassandra.query.scaladsl.CassandraReadJournal
import akka.persistence.journal.{EventAdapter, EventSeq, Tagged, WriteEventAdapter}
import akka.persistence.query.{Offset, PersistenceQuery}
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

import scala.collection.mutable

object PersistenceQueryDemo extends App {

  val system = ActorSystem("PersistenceQueryDemo", ConfigFactory.load().getConfig("custom-serializer"))

  val readJournal = PersistenceQuery(system).readJournalFor[CassandraReadJournal](CassandraReadJournal.Identifier)

  implicit val materializer = ActorMaterializer()(system)

  val persistenceIds = readJournal.currentPersistenceIds()
  persistenceIds.runForeach { persistenceId =>
    println(s"Found persistence ID: $persistenceId")
  }

    val events = readJournal.eventsByPersistenceId("guitar-manager-1", 0, Long.MaxValue)
    events.runForeach { event =>
      println(s"Read event: $event")
    }


  val eventsByTag = readJournal.eventsByTag("guitar-tag-2", Offset.noOffset)
  eventsByTag.runForeach { event =>
    println(s"Found a events by tag: $event")
  }

  val inventoryManager = system.actorOf(Props[InventoryManager], "simplePersistentActor")

  val guitars = for (i <- 1 to 1) yield Guitar(s"${i * 10}", s"CoolGuitar $i", "RockTheJVM")
  //      guitars.foreach { guitar =>
  //        inventoryManager ! AddGuitar(guitar, 5)
  //      }
  inventoryManager ! "print"

//  system.scheduler.scheduleOnce(5 seconds) {
//    val message = Guitar(s"21", s"Hakker 21", "RockTheJVM")
//    inventoryManager ! AddGuitar(message,1)
//  }

}


case class Guitar(id: String, model: String, make: String)

case class AddGuitar(guitar: Guitar, quantity: Int)

case class GuitarAdded(guitarId: String, guitarModel: String, guitarMake: String, quantity: Int)


class InventoryManager extends PersistentActor with ActorLogging {
  override def persistenceId: String = "guitar-manager-1"

  val inventory: mutable.Map[Guitar, Int] = new mutable.HashMap[Guitar, Int]()

  override def receiveCommand: Receive = {
    case AddGuitar(guitar@Guitar(id, model, make), quantity) =>
      persist(guitar) { _ =>
        addGuitarInventory(guitar, quantity)
        log.info(s"Added $quantity x $guitar to inventory")
      }
    case "print" =>
      log.info(s"Current inventory is: $inventory")
  }

  override def receiveRecover: Receive = {
    case event@GuitarAdded(id, model, make, quantity) =>
      log.info(s"Recovered $event")
      val guitar = Guitar(id, model, make)
      addGuitarInventory(guitar, 1)

    case event => println(event)
  }


  def addGuitarInventory(guitar: Guitar, quantity: Int) = {
    val existingQuantity = inventory.getOrElse(guitar, 0)
    inventory.put(guitar, existingQuantity + quantity)
  }
}


class GuitarsEventAdapter extends WriteEventAdapter {
  override def manifest(event: Any): String = "some-manifest"

  override def toJournal(event: Any): Any = event match {
    case event@Guitar(_, _, _) =>
      Tagged(event, Set("guitar-tag-2"))
    case event => event
  }
}

